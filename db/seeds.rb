# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Create a main sample user
require 'faker'
User.create!(name: "Example User",
  email: "example@railstutorial.org",
  password: "foobar",
  password_confirmation: "foobar")

#Generate a bunch of additional users.

99.times do |n|
  name1 = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name1,
    email: email,
    password: password,
    password_confirmation: password)

end

users=User.order(:created_at).take(10)
50.times do |n|
  content=Faker::Lorem.sentence(word_count: 5)
  users.each{|user| user.microposts.create!(content:content)}
end
