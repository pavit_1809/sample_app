class User < ApplicationRecord

  has_many :microposts, dependent: :destroy
  before_save {self.email=self.email.downcase}
  @VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :name, presence: true, length: {maximum: 50}
  validates :email, presence: true,length: {maximum:255},format: {with:@VALID_EMAIL_REGEX },uniqueness: true
  has_secure_password
  validates :password, presence: true, length: {minimum:6}

  # Returns the hash digest of the given string
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ?
    BCrypt::Engine::MIN_COST :
    BCrypt::Engine.cost
    BCrypt::Password.create(string,cost: cost)
  end
end


#although has_secure_password has a presence attribute but it show true flag for empty string also so we have to enforce a custom presence attribute to password field as well.
