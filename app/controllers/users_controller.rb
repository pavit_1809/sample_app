class UsersController < ApplicationController

  def show
    @user=User.find(params[:id])
  end

  def new
    @user=User.new
  end

  def create        #--> POST ROUTE
    @user = User.new(user_params)
    if (!!@user.save)
      log_in @user
      flash[:success]='Welcome to the Sample App!'
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
    @user=User.find(params[:id])
  end

  private

  def user_params #-> utility method needed to be implemented
    params.require(:user).permit(:name,:email,:password,:password_confirmation)
  end

end
