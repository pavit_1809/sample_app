class SessionsController < ApplicationController
  def new
  end

  def create   #-> creates a session for the successfully logged-in user
    #-> params is coming from input
    user=User.find_by(email: params[:session][:email].downcase)
    if (user && user.authenticate(params[:session][:password]))
      log_in(user) #->method defined in session helper
      redirect_to user #->Rails automatically converts it to the route for the user’s profile page
      #-> this is basically user_url(user)
    else
      #create an flash-error message
      flash.now[:danger]='Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

end
