require "test_helper"

class UserTest < ActiveSupport::TestCase
  def setup
    @user=User.new(name: "Example User",email: "user@example.com",password: "password", password_confirmation: "password")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name=""
    assert_not  @user.valid?
  end

  test "email should be present" do
    @user.email=""
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name="a" * 51
    assert_not  @user.valid?
  end

  test "email should not be too long" do
    @user.email="a"*245+"@example.com"
    assert_not  @user.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses=%w[foo@bar.com user@example.com first.last@goo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email=valid_address
      assert @user.valid? "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses=%w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email=invalid_address
      assert_not @user.valid? "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user=@user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  test "password should be present(non-blank)" do
    @user.password=@user.password_confirmation=""
    assert_not @user.valid?
  end

  test "password's length should be greater than 5" do
    @user.password=@user.password_confirmation="a" * 5
    assert_not @user.valid?
  end

  test "associated micropost should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem Ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end

end


#when checking for negative tests assert_not is used and failing tests are introduced to check if the validations are working fine
#when checking for positive tests assert is used and passing tests are introduced to check if the validations are fine
